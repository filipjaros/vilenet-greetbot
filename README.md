init 6 GreetBot
================

Implementation based on ViLeNet Chat Protocol 1

Documentation Spec: http://bit.ly/2iyjddR


Compiling & Running
===========
* Download and install apache maven: https://maven.apache.org/

* From greetbot directory run: `mvn clean package`

* This will create `greetbot.tar.gz` in folder `target`

* Unpack this tar.gz anywhere and open folder greebot.

* Edit greetbot.conf for your account

*  In the same folder run, open command prompt and run `java -jar greetbot.jar`
