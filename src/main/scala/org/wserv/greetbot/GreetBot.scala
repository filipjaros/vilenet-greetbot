package org.wserv.greetbot

import java.io.{File, InputStreamReader, BufferedReader, PrintWriter}
import java.net.Socket
import java.util.concurrent.{TimeUnit, Executors}

import com.typesafe.config.ConfigFactory

import scala.annotation.tailrec
import scala.util.Try

/**
  *  Implementation based on init 6 Chat Protocol 1
  *  Documentation Spec: http://bit.ly/2iyjddR
  */
object GreetBot extends App {

  val configName = "greetbot.conf"

  val config = ConfigFactory
    .parseFile(new File(configName))
    .withFallback(ConfigFactory.load(configName))
    .getConfig("GreetBot")

  val (server, port) = (config.getString("server"), config.getInt("port"))

  println(s"Connecting to $server:$port")

  val socket = new Socket(server, port)
  val socketWriter = new PrintWriter(socket.getOutputStream)
  val socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream))

  println("Connected!")

  // Function for writing messages
  def write(message: String) = {
    synchronized {
      socketWriter.write(s"$message\r\n")
      socketWriter.flush()
    }
  }

  // Loop for reading messages from a buffered reader
  @tailrec
  def readMessageLoop(reader: BufferedReader, readFunction: String => Any): Unit = {
    val message = reader.readLine()
    if (message != null) {
      readFunction(message)
      readMessageLoop(reader, readFunction)
    } else {
      println("Connection error!")
    }
  }

  // Thread for reading messages
  val readerThread = Executors.newSingleThreadExecutor()
  readerThread.submit(new Runnable {
    override def run() = readMessageLoop(socketReader, message => {
      println(parseMessage(message))
    })

    def parseMessage(message: String): String = {
      val splt = message.split(" ", 2)
      val Array(command, rest) =
        if (splt.length > 1) {
          splt
        } else {
          Array(splt.head, "")
        }

      Try {
        // match command group to how many spaced columns it has.
        // split accordingly and call the group parsing function
        val (func, columns) =
          command match {
            case "OK" => parseOKCommand -> 1
            case "FAIL" => parseFailCommand -> 1
            case "USER" => parseUserCommand -> 7
            case "CHANNEL" => parseChannelCommand -> 5
            case "SERVER" => parseServerCommand -> 5
            case "PING" => parsePingCommand -> 1
          }
        func(rest.split(" ", columns))
      }.getOrElse(parseInvalidCommand(command, message))
    }

    def parseOKCommand = (splt: Array[String]) => {
      s">> Successfully logged in."
    }

    def parseFailCommand = (splt: Array[String]) => {
      s">> Failed to login. Reason: ${splt.head}"
    }

    def parsePingCommand = (splt: Array[String]) => {
      s"/PONG ${splt.head}"
    }

    def parseUserCommand = (splt: Array[String]) => {
      // unwrap array
      val Array(command, direction, reserved2, flags, ping, name, message) = splt

      // parse command to a readable string
      command match {
        case "IN" => s">> $name is in the channel with flags $flags and ping $ping."
        case "JOIN" =>
          // The Greet!
          write(s"Hi $name! Welcome to init 6!")
          s">> $name joined the channel with flags $flags and ping $ping."
        case "LEAVE" => s">> $name left the channel."
        case "TALK" =>
          direction match {
            case "FROM" => s"<$name> $message"
            case "TO" => s"<$name> $message"
          }
        case "WHISPER" =>
          direction match {
            case "FROM" => s"<From: $name> $message"
            case "TO" => s"<To: $name> $message"
          }
        case "EMOTE" => s"<$name $message>"
        case "UPDATE" => s"$name has been updated to flags $flags and ping $ping."
      }
    }

    def parseChannelCommand = (splt: Array[String]) => {
      // unwrap array
      val Array(command, reserved1, reserved2, flags, message) = splt

      // parse command to a readable string
      command match {
        case "JOIN" => s">> You have joined channel $message with flags $flags."
        case "TOPIC" => s">> Channel topic: $message"
      }
    }

    def parseServerCommand = (splt: Array[String]) => {
      // unwrap array
      val Array(command, reserved1, reserved2, name, message) = splt

      // parse command to a readable string
      command match {
        case "INFO" => s">> Info: $message"
        case "BROADCAST" => s">> Broadcast <$name> $message"
        case "TOPIC" => s">> init 6: $message"
        case "ERROR" => s">> Error: $message"
      }
    }

    def parseInvalidCommand(group: String, message: String) = s"Received invalid $group command: $message"
  })

  // keep alive thread will send a NULL packet every 1 minute
  val keepAlivePackeThread = Executors.newSingleThreadScheduledExecutor()
  keepAlivePackeThread.scheduleWithFixedDelay(new Runnable {
    override def run(): Unit = {
      write("/NULL")
    }
  }, 30 /* Initial Delay */, 60 /* Delay after initial delay */, TimeUnit.SECONDS)

  // Packet to connect
  val username = config.getString("username")
  val password = config.getString("password")
  val home = config.getString("home")

  write("C1") // Protocol Header C1
  write(s"ACCT $username") // Account Name
  write(s"PASS $password") // Account Password
  write(s"HOME $home") // Home Channel
  write("LOGIN") // Tell server to login

  // Read from console
  readMessageLoop(Console.in, message => {
    if (message.toLowerCase == "/quit") {
      keepAlivePackeThread.shutdownNow()
      readerThread.shutdownNow()
      socketWriter.close()
      socketReader.close()
      sys.exit()
    } else {
      write(message)
    }
  })
}
